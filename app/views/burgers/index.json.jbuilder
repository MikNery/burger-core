json.array!(@burgers) do |burger|
  json.extract! burger, :id, :name, :price, :description, :image_url
  json.url burger_url(burger, format: :json)
end
