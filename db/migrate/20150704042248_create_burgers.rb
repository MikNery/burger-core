class CreateBurgers < ActiveRecord::Migration
  def change
    create_table :burgers do |t|
      t.string :name
      t.float :price
      t.text :description
      t.string :image_url

      t.timestamps null: false
    end
  end
end
